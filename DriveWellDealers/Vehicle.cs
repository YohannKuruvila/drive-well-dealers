﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DriveWellDealers
{
    class Vehicle
    {
        public int _id { get; set; }
        public string _make { get; set; }
        public int _purchasePrice { get; set; }
        public int _age { get; set; }
        public int _Rate { get; set; }
        public int _miles { get; set; }
        public Depreciation _deprRate { get; set; }

        /// <summary>
        /// Constructor for the vehicle class.
        /// </summary>
        /// <param name="ID">ID parameter (int)</param>
        /// <param name="Make">Car Make (string)</param>
        /// <param name="PurchasePrice">Purchase Price (int)</param>
        /// <param name="age">Age (int)</param>
        /// <param name="deprRate">Depreciation Rate (int)</param>
        /// <param name="mileage">Mileage (int)</param>
        public Vehicle(int ID, string Make, int PurchasePrice, int age, int deprRate, int mileage)
        {
            _id = ID;
            _make = Make;
            _purchasePrice = PurchasePrice;
            _age = age;
            _miles = mileage;
            _Rate = deprRate;
        }
    
        /// <summary>
        /// This function find the depreciation value, and returns it.
        /// </summary>
        /// <returns>Returns the double number value of what the depreciation value is</returns>
        public double FindDepr() {
            Depreciation _depr = new Depreciation(_age, _miles, _Rate);
            double totalDep = _depr.DepreciationValue;
            return totalDep;
        }
    }
}
