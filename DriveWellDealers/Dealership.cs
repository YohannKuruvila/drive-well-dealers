﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DriveWellDealers
{
    class Dealership
    {
        List<Vehicle> vehicleList = new List<Vehicle>();

        /// <summary>
        /// Initializes the dealership class.
        /// </summary>
        public Dealership()
        {
        }
        /// <summary>
        /// Function to record Vehicle information. Parameters are fairly self-explanatory.
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="Make"></param>
        /// <param name="PurchasePrice"></param>
        /// <param name="age"></param>
        /// <param name="deprRate"></param>
        /// <param name="miles"></param>
        /// <returns></returns>
        public int RecordVehicleInformation(int ID, string Make, int PurchasePrice, int age, int deprRate, int miles) {
            Vehicle car = new Vehicle(ID: ID, Make: Make, PurchasePrice: PurchasePrice, age: age,deprRate:deprRate, mileage:miles );
            if (VehicleAdd(car) == 1)
            {
                return 1;
            }
            return 0;
            
        }

        /// <summary>
        /// Searches for the ID in the list of vehicles if inside.
        /// </summary>
        /// <param name="ID">The integer ID of the car you are looking for.</param>
        /// <returns>The vehicle you are looking for, if not found, then null</returns>
        public Vehicle IDSearch(int ID)
        {
            foreach (Vehicle vehicle in vehicleList)
            {
                if (vehicle._id == ID)
                    return vehicle;
            }
            return null;
        }

        /// <summary>
        /// Function that adds a vehicle to the list.
        /// </summary>
        /// <param name="vehicle">Self-explanatory</param>
        /// <returns>1 if failure, 0 if success</returns>
        public int VehicleAdd(Vehicle vehicle)
        {
            if (IDSearch(vehicle._id) == null)
            {
                vehicleList.Add(vehicle);
                return 0;
            }
            return 1;
        }

        /// <summary>
        /// Calculate the Total Value of every vehicle in the lot.
        /// </summary>
        /// <returns>The Double value of every vehicle contained in the lot</returns>
        public double CalculateTotalValue()
        {
            double totalCost = 0;

            foreach (var car in vehicleList)
            {
                double carDep = car.FindDepr();
                totalCost += carDep;
            }
            return totalCost;
        }

        /// <summary>
        /// Gives you the total price after depreciation. 
        /// </summary>
        /// <returns>The Double value of how much the cars on the lot are worth</returns>
        public double DepreciationPrice()
        {
            double total = 0;
            foreach (var car in vehicleList)
            {
                double carDepr = car.FindDepr();
                double DeprLoss = carDepr / 100;
                double carValue = car._purchasePrice - (car._purchasePrice * DeprLoss);
                total += carValue;
            }
            return total;
        }

        public void SmallSort()
        {
            vehicleList.Sort((b, a) => (a.FindDepr().CompareTo(b.FindDepr())));
        }

        public void LargeSort()
        {
            vehicleList.Sort((a,b) => (a.FindDepr().CompareTo(b.FindDepr())));
        }

        /// <summary>
        /// Provides an instance of the Lot to the outside functions.
        /// </summary>
        /// <returns>The list of items contained inside the dealership.</returns>
        public List<Vehicle> LotProvider()
        {
            return vehicleList;
        }
    }
}
