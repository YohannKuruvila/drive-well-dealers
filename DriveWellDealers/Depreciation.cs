﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DriveWellDealers
{
    class Depreciation
    {

        public int _model { get; set; }
        public int _miles { get; set; }
        public int _deprRate { get; set; }

        const double year = 0.10;
        const double mile = 0.025;


        public Depreciation(int Model, int Miles, int deprRate)
        {
            _model = Model;
            _miles = Miles;
            _deprRate = deprRate;
        }

        public double DepreciationValue
        {
            get
            {
                var carAge = _model;
                var YearDep = (carAge) * year;

                var ThousandCount = (int)(Math.Round(_miles / 10000d));
                var MilesDep = ThousandCount * _miles;

                var TotalDep = YearDep + MilesDep + _deprRate;

                return TotalDep;

            }
        }
    }
}
