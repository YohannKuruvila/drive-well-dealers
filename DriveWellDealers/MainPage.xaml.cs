﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Popups;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace DriveWellDealers
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {

        Dealership DriveWell;

        public MainPage()
        {
            this.InitializeComponent();

            DriveWell = new Dealership();
        }

        /// <summary>
        /// The function that submits Info to the DriveWell Dealership class.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void InfoButtonClick(object sender, RoutedEventArgs e) {
            if ((string.IsNullOrWhiteSpace(txtID.Text)) || (string.IsNullOrWhiteSpace(CarMake.Text)) || (string.IsNullOrWhiteSpace(txtMiles.Text)) || (string.IsNullOrWhiteSpace(depreciateRate.Text)))
            {
                var MessageDlg = new MessageDialog("Invalid Input Data.");
                await MessageDlg.ShowAsync();
            }

            else
            {
                int ID = int.Parse(txtID.Text);
                string Make = CarMake.Text;
                int Price = int.Parse(PurchasePrice.Text);
                int Age = int.Parse(txtAge.SelectedIndex.ToString());
                int Miles = int.Parse(txtMiles.Text);
                int deprRate = int.Parse(depreciateRate.Text);
                if (DriveWell.RecordVehicleInformation(ID, Make, Price, Age, deprRate, Miles) == 1)
                {
                    var MessageDlg = new MessageDialog("Car already exists in inventory.");
                    await MessageDlg.ShowAsync();
                }
            }




            
        }

        /// <summary>
        /// The function that governs what happens when you hit the report button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void ReportBClicked(object sender, RoutedEventArgs e)
        {
            var TotalValue = DriveWell.DepreciationPrice();
            var MessageDlg = new MessageDialog("The total depreciatied value of cars in this field is $" + TotalValue);
            await MessageDlg.ShowAsync();
            DriveWell.LargeSort();
            var LowToHighIntro = new MessageDialog("Cars will now be sorted according to smallest to highest depreciation.");
            await LowToHighIntro.ShowAsync();
            var LowList = DriveWell.LotProvider();
            foreach (Vehicle car in LowList)
            {
                var LowLoop = new MessageDialog("ID:" + car._id + "\n" + "Make:" + car._make + "\n" + "Purchase Price: $" + car._purchasePrice + "\n" + "Age:" + car._age + "\n" + "Depreciation Rate:" + car._Rate + "\n" + "Miles Travelled:" + car._miles + "\n" + "Depreciated Value:" + (car._purchasePrice - (car._purchasePrice * (car.FindDepr() / 100))));
                await LowLoop.ShowAsync();                    
            }
            DriveWell.SmallSort();
            var HightoLowIntro = new MessageDialog("Cars will now be sorted according to highest to smallest depreciation.");
            await HightoLowIntro.ShowAsync();
            var HighList = DriveWell.LotProvider();
            foreach (Vehicle car in HighList)
            {
                var HighLoop = new MessageDialog("ID:" + car._id + "\n" + "Make:" + car._make + "\n" + "Purchase Price: $" + car._purchasePrice + "\n" + "Age:" + car._age + "\n" + "Depreciation Rate:" + car._Rate + "\n" + "Miles Travelled:" + car._miles + "\n" + "Depreciated Value:" + (car._purchasePrice - (car._purchasePrice * (car.FindDepr() / 100))));
                await HighLoop.ShowAsync();
            }
        }
    }


}
